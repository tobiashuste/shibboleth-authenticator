Version 0.1.2

- Add `.rodare.json` file for automatic software preservation [#46](https://github.com/tobiasfrust/shibboleth-authenticator/pull/46)

Version 0.1.1

- Create linked identity when user logs in to support multiple idenitiy providers [#45](https://github.com/tobiasfrust/shibboleth-authenticator/pull/45)

Version 0.1

- Initial release
